#!/bin/bash

#brew install cuetools flac ffmpeg shntool
find . -maxdepth 3 -mindepth 1 -type f -name '*.cue' -print0 | while IFS= read -r -d '' f; do
	dirName=$(dirname "$f")
	fileName=$(basename "$f")
	fileName="${fileName%.*}"
	tmpfile=$(mktemp)
	iconv -f iso-8859-1 -t utf-8 "${dirName}/${fileName}.cue" > $tmpfile
	{       
		shntool split -O always -f $tmpfile -o flac -d "${dirName}" -t '%n - %p - %t' "${dirName}/${fileName}.flac" &&
		rsync -R "${dirName}/${fileName}.flac" $HOME/backup/ &&
		rm "${dirName}/${fileName}.flac"
	} || {
	echo "something went wrong"
	}
	rm $tmpfile
done
