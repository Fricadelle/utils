#!/bin/bash

INPUT=$1

filename=$(basename -- "$INPUT")
extension="${filename##*.}"
filename="${filename%.*}"

SILENCE_DETECT="silence_detect_logs.txt"
TIMESTAMPS="start_timestamps.txt"

if [ ! -f $TIMESTAMPS ]; then
    echo "Probing start timestamps"
    ffmpeg -i "$INPUT" -af "silencedetect=n=-50dB:d=3" -f null - 2> "$SILENCE_DETECT"
    cat "$SILENCE_DETECT"| grep "silence_start: [0-9.]*" -o| grep -E '[0-9]+(?:\.[0-9]*)?' -o > "$TIMESTAMPS"
fi

PREV=0.0
number=0
tx=0.0
cat "$TIMESTAMPS"| ( while read ts
do
    printf -v fname -- "$filename-%02d.$extension" "$(( ++number ))"
    DURATION=$( bc <<< "$ts - $PREV")
    echo $PREV
    ffmpeg -y -ss "$PREV" -i "$INPUT" -t "$DURATION" -c copy "$fname" < /dev/null
    PREV=$ts
done
printf -v fname -- "$filename-%02d.$extension" "$(( ++number ))"
ffmpeg -y -ss "$PREV" -i "$INPUT" -c copy "$fname" < /dev/null)