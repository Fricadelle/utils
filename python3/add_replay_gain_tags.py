import os
from subprocess import run
from glob import glob, escape
import argparse
#pip3 install mutagen
def main(folder):
    
    list_fn = glob(escape(folder) + '/**/*.flac', recursive=True)
    dirs = set([os.path.dirname(path) for path in list_fn])

    for d in dirs:
        flac_files = [x for x in glob(escape(d)+"/*.flac")]
        print(d)
        run(['metaflac', '--add-replay-gain'] + flac_files)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='add replay gain to subfolders contained in specified folder')
    parser.add_argument('directory', help='folder that contains albums with flac files', action='store')
    args = parser.parse_args()
    
    main(args.directory)