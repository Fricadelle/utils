import java.io.{File, _}

import org.apache.commons.io.FileUtils
import org.apache.commons.io.filefilter.TrueFileFilter
import org.jaudiotagger.audio.{AudioFile, AudioFileIO}
import org.jaudiotagger.tag.Tag

import scala.collection.JavaConverters._

object TagExtractor {

  def extractMetaData(tag: Tag): Map[String, String] = {
    tag.getFields().asScala.map(t => (t.getId -> t.toString)).toMap.withDefaultValue("")
  }

  def writeToCsv(fields: collection.mutable.ListBuffer[String]): Unit = {
    new PrintWriter("filename") {
      fields.foreach {
        case s =>
          write(s)
          write("\n")
      }
      close
    }
  }

  def main(args: Array[String]): Unit = {
    val dirName = "C:\\Users\\l.plaetevoet\\Music\\My Music"
    val sepField = "\t"
    val dir = new java.io.File(dirName)
    val files: List[File] = FileUtils
      .listFiles(dir, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE).asScala.toList
      .filter(_.getName.endsWith(".flac"))

    val records = collection.mutable.ListBuffer.empty[String]

    val header = List("ARTIST", "ALBUM", "DATE", "REPLAYGAIN_ALBUM_GAIN")
    val data = collection.mutable.ListBuffer[String](header.mkString(sepField))
    files.foreach { f => {
        val audioFile: AudioFile = AudioFileIO.read(f)
        val tag: Tag = audioFile.getTag
        val m = extractMetaData(tag)
        val extractedValues = header map m
        data += extractedValues.mkString(sepField)
      }
    }
    writeToCsv(data.distinct)
  }
}
